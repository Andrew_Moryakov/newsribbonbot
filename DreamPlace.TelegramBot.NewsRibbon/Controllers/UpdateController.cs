using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DreamPlace.TelegramBot.NewsRibbon.Models;
using Microsoft.AspNetCore.Mvc;
using Telegram.Bot.Types;

namespace DreamPlace.TelegramBot.NewsRibbon.Controllers
{
	[Route("api/update")]
	public class UpdateController : Controller
	{
		[HttpPost]
		public async Task<OkResult> Post([FromBody] Update update)
		{
			var comMands = Bot.Commands;
			var message = update.Message;
			var client = await Bot.GetClientAsync();

			foreach (var command in comMands)
			{
				if (command.Contains(message.Text))
				{
					command.Execute(message, client);
					break;
				}
			}

			return Ok();
		}

		[HttpGet]
		public async Task<OkResult> Get([FromBody] Update update)
		{
			var comMands = Bot.Commands;
			var message = update.Message;
			var client = await Bot.GetClientAsync();

			foreach (var command in comMands)
			{
				if (command.Contains(message.Text))
				{
					command.Execute(message, client);
					break;
				}
			}

			return Ok();
		}

	}
}