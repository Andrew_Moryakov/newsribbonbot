﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot;

namespace DreamPlace.TelegramBot.NewsRibbon.Models.Commands
{
    public abstract class Command
    {
		public abstract string Name { get; }
	    public abstract void Execute(Message message, TelegramBotClient client);

	    public bool Contains(string comand)
	    {
		    if (comand.Contains(this.Name))
		    {
			    return true;
		    }
		    return false;
	    }
    }
}
