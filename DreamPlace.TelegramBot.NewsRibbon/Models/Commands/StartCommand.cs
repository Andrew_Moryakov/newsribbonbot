﻿using Telegram.Bot;
using Telegram.Bot.Types;

namespace DreamPlace.TelegramBot.NewsRibbon.Models.Commands
{
    public class StartCommand: Command
    {
	    public override string Name => "start";
	    public override async void Execute(Message message, TelegramBotClient client)
	    {
		    var chatId = message.Chat.Id;
		    var messId = message.MessageId;

		    await client.SendTextMessageAsync(chatId, "Скоро запуск", replyToMessageId: messId);
		    await client.SendPhotoAsync(chatId, "https://avatars2.githubusercontent.com/u/6422482?s=460&v=4", replyToMessageId: messId);
	    }
	}
}
