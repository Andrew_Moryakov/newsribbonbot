﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DreamPlace.TelegramBot.NewsRibbon.Models.Commands;
using Telegram.Bot;
using AppSettings = DreamPlace.TelegramBot.NewsRibbon.Models.AppSettings;

namespace DreamPlace.TelegramBot.NewsRibbon.Models
{
	public static class Bot
	{
		private static TelegramBotClient _client;
		private static List<Command> _commandList;
		public static IReadOnlyList<Command> Commands
		{
			get => _commandList.AsReadOnly();
		}

		public static async Task<TelegramBotClient> GetClientAsync()
		{
			if (_client != null)
			{
				return _client;
			}

			_commandList = new List<Command>();
			_commandList.Add(new StartCommand());
			//ToDo Добавлять тут команды

			{
				_client = new TelegramBotClient(AppSettings.Key);
				var hook = $"{AppSettings.BotUrl}api/update";
				await _client.SetWebhookAsync(hook);
				return _client;
			}
		}
	}
}